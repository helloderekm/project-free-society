package controllers;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import models.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import services.PostService;


public class PostDataController {

    private Logger logger = LoggerFactory.getLogger(PostDataController.class);
    private PostService postService = new PostService();


    public void handleGetPostsRequest(Context context) {
        logger.info("getting all posts");
        context.json(postService.getAll());
    }

    public void handleGetPostByIdRequest(Context context) {
        String postIdString = context.pathParam("id");

        if(postIdString.matches("^\\d+$")){
            int postIdInput = Integer.parseInt(postIdString);

            Post post = postService.getById(postIdInput);

            if(post == null){
                logger.warn("no post present with id: " + postIdInput);
                throw new NotFoundResponse("No post found with provided ID: " + postIdInput);
            } else {
                logger.info("getting post with id: " + postIdInput);
                context.json(post);
            }
        } else {
            throw new BadRequestResponse("id input: \"" + postIdString + "\"cannot be parsed to an int");
        }
    }

    public void handleCreatePostRequest (Context context){
        Post post = context.bodyAsClass(Post.class);
        logger.info("adding new item: " + post);
        postService.add(post);
        context.status(201);
    }

    public void handleUpdatePostByIdRequest(Context context){
        String postIdString = context.pathParam("id");
        Post post = context.bodyAsClass(Post.class);

        if(postIdString.matches("^\\d+$")){
            int postIdInput = Integer.parseInt(postIdString);
            logger.info("updating content of post with postId: " + postIdInput);
            postService.update(postIdInput,post);
        }

    }

    public void handleDeletePostByIdRequest(Context context){


        String postIdString = context.pathParam("id");
        if(postIdString.matches("^\\d+$")){

            int postIdInput = Integer.parseInt(postIdString);
            logger.info("deleting post with id: " + postIdInput);
            postService.delete(postIdInput);

        } else{
            throw new BadRequestResponse("input \""+postIdString+"\" cannot be parsed to an int");
        }



    }


}
