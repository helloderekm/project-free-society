package models;

public class Post {

    private int postId;
    private String title;
    private int userId;
    private String postBody;
   // private List<Comment> commentList;


   /* public Post(int postId, String title, int userId, String date, String postBody){
        this.postId = postId;
        this.title = title;
        this.userId = userId;
        this.date = date;
        this.postBody = postBody;
        commentList = new ArrayList<Comment>();;
    }*/

    public Post(){
        super();
    }

    public Post(int postId, String title, int userId, String postBody) {
        this.postId = postId;
        this.title = title;
        this.userId = userId;
        this.postBody = postBody;
    }

 /*   public void addComment(int id, User user, Date date, String message){
        Comment newComment = new Comment(this.postId, id, user,date,message);
        commentList.add(newComment);
    }*/

    public void sortComment(){
        // sorts comment
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getPostBody() {
        return postBody;
    }

    public void setPostBody(String postBody) {
        this.postBody = postBody;
    }

    /*public void displayCommentList(){

        if(commentList.isEmpty()){
            System.out.println();
        }

        for(Comment comment : commentList) {
            System.out.println("\n    {   postId: " + comment.getPostId());
            System.out.println("        id: " + comment.getId());
            System.out.println("        user: " + comment.getUser());
            System.out.println("        date: " + comment.getDate());
            System.out.println("        body: " + comment.getCommentBody() + "\n     }");
        }
    }*/

}
