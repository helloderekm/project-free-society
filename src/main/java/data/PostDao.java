package data;

import models.Post;
import java.util.List;

public interface PostDao {

    public List<Post> getAllItems();
    public Post getPostById(int postId);
    public Post addNewPost(Post post);
    public void deletePost(int postId);
    public void updatePost(int postId, Post post);


}
