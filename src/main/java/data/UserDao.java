package data;

import models.Post;
import models.User;

import java.util.List;

public interface UserDao {
    List<User> getAllUsers();
    User getUserById(int userId);
    User addNewUser(User user);
    void deleteUser(int userId);
    void updateUser(int userId, User user);
    List<Post> getUserPostsByUserId(int userId);
}
