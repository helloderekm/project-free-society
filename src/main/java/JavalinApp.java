import controllers.AuthController;
import controllers.PostDataController;
import controllers.UserDataController;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;
import static io.javalin.apibuilder.ApiBuilder.get;

public class JavalinApp {


    PostDataController postDataController = new PostDataController();
    AuthController authController = new AuthController();
    UserDataController userDataController = new UserDataController();


    Javalin app = Javalin.create().routes(() -> {

        path("/posts", () -> {
            before("/", authController::authorizeToken);
            get(postDataController::handleGetPostsRequest);
            post(postDataController::handleCreatePostRequest);
            path("/:id", () -> {
                before("/", authController::authorizeToken);
                get(postDataController::handleGetPostByIdRequest);
                put(postDataController::handleUpdatePostByIdRequest);
                delete(postDataController::handleDeletePostByIdRequest);
            });


        });

        path("/login", () -> {
            post(authController::authenticateLogin);
        });

        path("/users", () -> {
            before("/", authController::authorizeToken);
            get(userDataController::handleGetAllUsersRequest);
            post(userDataController::handleCreateUserRequest);
            path("/:id", () -> {
                before("/", authController::authorizeToken);
                get(userDataController::handleGetUserByIdRequest);
                put(userDataController::handleUpdateUserByIdRequest);
                delete(userDataController::handleDeleteUserByIdRequest);

                path("/posts", () -> {
                    before("/", authController::authorizeToken);
                    get(userDataController::handleGetPostsByUserIdRequest);
                });
            });
        });

    });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
